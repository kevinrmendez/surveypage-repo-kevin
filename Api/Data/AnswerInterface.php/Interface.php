<?php
namespace Survey\SurveyPage\Api\Data;
interface AnswerInterface
{
   const ANSWER_ID = 'answer_id';
   const HAPPY = 'happy';
   const HUNGRY = 'hungry';
   const TIRED = 'tired';
   const SAD = 'sad';
   const PRODUCT_ID = 'product_id';

   public function getId();
   public function getHungry();
   public function getHappy();
   public function getTired();
   public function getSad();
   public function setId($value);
   public function setHungry($value);
   public function setHappy($value);
   public function setTired($value);
   public function setProductId($product_id);
}
